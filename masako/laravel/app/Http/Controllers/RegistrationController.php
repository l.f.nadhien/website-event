<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function create(){
        return view('registrasi.create');
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'nik' => 'required|size:8,unique:Registrations',
            'nama' => 'required|min:3|max:50',
            'alamat' => 'required',
            'no_hp' => 'required|',
            'email' => 'required',
            'password' => '',
        ]);

        $registration = new Registration();
        $registration->nik = $validateData['nik'];
        $registration->nama = $validateData['nama'];
        $registration->alamat = $validateData['alamat'];
        $registration->no_ho = $validateData['no_hp'];
        $registration->email = $validateData['email'];
        $registration->password = $validateData['password'];
        $registration->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('registration$registration.index');
    }
}
